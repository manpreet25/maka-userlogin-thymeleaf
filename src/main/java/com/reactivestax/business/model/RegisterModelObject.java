package com.reactivestax.business.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public class RegisterModelObject {
    @NotBlank(message = "username cannot be empty")
    @Length(min = 5, max = 10, message = "UserName length has to be between 5 and 10")
    private String userName;

    @NotBlank(message = "password cannot be empty")
    @Length(min = 5, max = 10, message = "Password length has to be between 5 and 10")
    private String password;


    private Integer phone;

    @NotBlank(message = "email  cannot be empty")
    private String email;

    @NotBlank(message = "Firstname   cannot be empty")
    private String firstName;

    @NotBlank(message = "Lastname   cannot be empty")
    private String lastName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Override
    public String toString() {
        return "RegisterModelObject{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
