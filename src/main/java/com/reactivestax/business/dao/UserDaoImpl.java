package com.reactivestax.business.dao;
import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.RegisterModelObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class UserDaoImpl implements UserDao

{

    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void register(RegisterModelObject registerModelObject) {

        String sql = "insert into users values(?,?,?,?,?,?)";

        jdbcTemplate.update(sql, new Object[] { registerModelObject.getUserName(), registerModelObject.getPassword(),registerModelObject.getFirstName(),
                registerModelObject.getLastName(), registerModelObject.getEmail(), registerModelObject.getPhone() });
    }

    public RegisterModelObject validateUser(LoginModelObject loginModelObject) {

        String sql = "select * from users where username='" + loginModelObject.getUserName() + "' and password='" + loginModelObject.getPassword()
                + "'";

        List<RegisterModelObject> users = jdbcTemplate.query(sql, new UserMapper());

        return users.size() > 0 ? users.get(0) : null;
    }
    class UserMapper implements RowMapper<RegisterModelObject> {

        public RegisterModelObject mapRow(ResultSet rs, int arg1) throws SQLException {
            RegisterModelObject user = new RegisterModelObject();

            user.setUserName(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("firstname"));
            user.setLastName(rs.getString("lastname"));
            user.setEmail(rs.getString("email"));
            user.setPhone(rs.getInt("phone"));

            return user;
        }
    }
}
