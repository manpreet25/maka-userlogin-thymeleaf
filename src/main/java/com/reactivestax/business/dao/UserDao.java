package com.reactivestax.business.dao;

import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.RegisterModelObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public interface UserDao {


    void register(RegisterModelObject registerModelObject);

    RegisterModelObject validateUser(LoginModelObject loginModelObject);
}
