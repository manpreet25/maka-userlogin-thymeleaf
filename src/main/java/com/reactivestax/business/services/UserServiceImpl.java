package com.reactivestax.business.services;

import com.reactivestax.business.dao.UserDao;
import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.RegisterModelObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class UserServiceImpl  implements  UserService{


        @Autowired
        public UserDao userDao;

        @Override
        public void registerUser(RegisterModelObject user) {
            userDao.register(user);
        }

        @Override
        public RegisterModelObject validateUser(LoginModelObject login) {
            return userDao.validateUser(login);
        }
}
