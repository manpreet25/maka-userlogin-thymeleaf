package com.reactivestax.business.services;


import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.RegisterModelObject;

public interface UserService {

        void registerUser(RegisterModelObject user );
        RegisterModelObject validateUser(LoginModelObject login);

    }


