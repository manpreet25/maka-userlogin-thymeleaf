package com.reactivestax.web.controller;


import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.RegisterModelObject;
import com.reactivestax.business.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;



@Controller
public class MainController {

    public MainController() {
        super();
    }

    @Autowired
    UserService userService;

    @RequestMapping({"/"})
    public ModelAndView baseRequestMapping()
    {
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }


    @RequestMapping({"/login"})
    public ModelAndView loginRequestMapping()
    {
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("loginModelObject",new LoginModelObject());
        return modelAndView;
    }


//    @RequestMapping({"/loginProcess"})
//    public ModelAndView loginProcessRequestMapping(    @ModelAttribute("login")  @Valid final LoginModelObject loginModelObject, BindingResult bindingResult){
//        if(bindingResult.hasErrors()){
//            ModelAndView modelAndView = new ModelAndView("login");
//            System.out.println("login binding errors");
//            return modelAndView;
//        }
//        ModelAndView modelAndView = new ModelAndView();
//        RegisterModelObject registerModelObject = userService.validateUser(loginModelObject);
//        if(null !=registerModelObject)
//        {
//            modelAndView = new ModelAndView("welcome");
//            modelAndView.addObject("firstName", registerModelObject.getFirstName());
//
//        }
//        else {
//            modelAndView= new ModelAndView("login");
//            modelAndView.addObject("message","username or password is wrong");
//        }
//
//      return  modelAndView;
//
//    }

    @RequestMapping({"/loginProcess"})
    public ModelAndView loginProcessRequestMapping(@Valid final LoginModelObject loginModelObject, BindingResult bindingResult) {
        /**field level validation**/
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("login");
            return modelAndView;
        }
        //ModelAndView modelAndView = new ModelAndView();
        ModelAndView mav = null;
        RegisterModelObject user = userService.validateUser(loginModelObject);
        if (null != user) {
            mav = new ModelAndView("welcome");
            mav.addObject("firstName", user.getFirstName());
        } else {
            mav = new ModelAndView("login");
            System.out.println("going to login.jsp");
            mav.addObject("message", "Username or Password is wrong!!");
        }
        return mav;
    }

    @RequestMapping({"/register"})
    public ModelAndView registerRequestMapping()
    {
        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("registerModelObject", new RegisterModelObject());
        return modelAndView;
    }


    @RequestMapping({"/registerProcess"} )

    public ModelAndView registerProcessRequestMapping(@Valid final RegisterModelObject registrationModelObject,
                                                      BindingResult bindingResult,
                                                      HttpServletRequest httpServletRequest) {

        System.out.println("regisrationModelObject=" + registrationModelObject);

        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("register");
            System.out.println("inside binding errors");
            return modelAndView;
        }
        userService.registerUser(registrationModelObject);
        httpServletRequest.setAttribute("message", "registration successfully done ..wow");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forward:/login");
        return modelAndView;
    }


    @RequestMapping({"/home"})
    public ModelAndView homeRequestMapping()
    {
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }
}